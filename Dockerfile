ARG OPENJDK_VERSION=16-alpine
FROM openjdk:${OPENJDK_VERSION}

ENV USER=wabuser
ENV GROUP=wabgroup
ENV UID=1000
ENV GID=1000

RUN addgroup --system ${GROUP} --gid ${GID} && \
	adduser --system ${USER} --ingroup ${GROUP} --uid ${UID} && \
	apk add npm git && \
	npm install -g esri-wab-build bower

USER ${USER}